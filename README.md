## Mennion.Common ##

Knihovna nástrojů a rozšíření pro použití v asp net mvc aplikací. (.net45 a mvc 4)

Knihovna obsahuje upravený zdrojový kod od Bena Fostera [Fabrik.Common](https://github.com/benfoster/Fabrik.Common)

Více informací na [wiki](https://bitbucket.org/Mennion/mennion.common/wiki/Home)


