using System;
using System.Globalization;

namespace Mennion.Common.Extensions
{
    /// <summary>
    /// Roz���en� datov�ho typu Decimal
    /// </summary>
    public static class DecimalExtensions
    {
        /// <summary>
        /// Porovn�v� decimal ��slo zda se jedn� o cel� ��slo bez desetin� ��sti
        /// </summary>
        /// <param name="decimalToCheck">��slo k porovn�n�</param>
        /// <returns>true pokud ��slo obsahuje desetinnou ��st, jinak false</returns>
        public static bool IsDecimal(this decimal decimalToCheck)
        {
            return decimalToCheck.ToString(CultureInfo.InvariantCulture).IndexOf('.') != -1;
        }

        /// <summary>
        /// Vrac� ��slo bez desetinn� ��rky p�etypovan� na int
        /// </summary>
        /// <param name="decimalToCheck">��slo k porovn�n�</param>
        /// <returns>��slo v int</returns>
        public static int GetIntegerPart(this decimal decimalToCheck)
        {
            string[] parts = decimalToCheck.ToString(CultureInfo.InvariantCulture).Split(new[] { '.' });

            return Convert.ToInt32(parts[0]);
        }
        
    }
}