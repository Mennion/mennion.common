using System;
using System.Collections.Generic;
using System.Globalization;

namespace Mennion.Common.Extensions
{
    /// <summary>
    /// Roz���en� datov�ho typu DateTIME
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Vrac� poku� se zadadn� datum nach�z� v intervalu dat
        /// </summary>
        /// <param name="dateToCheck">datum kter� chceme otestovat</param>
        /// <param name="startDate">po��tek intervalu</param>
        /// <param name="endDate">konec intervalu</param>
        /// <returns>true pokud se zadan� datum nach�z� v intervalu, jinak false</returns>
        public static bool InRange(this DateTime dateToCheck, DateTime startDate, DateTime endDate)
        {
            return dateToCheck >= startDate && dateToCheck <= endDate;
        }

        /// <summary>
        /// Vrac� seznam m�s�cu ve form�tu (��slo-m�s�ce, n�zev-m�s�ce)
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<int, string>> GetAllMonths()
        {
            for (var i = 1; i <= 12; i++)
            {
                if (DateTimeFormatInfo.CurrentInfo != null)
                    yield return new KeyValuePair<int, string>(i, DateTimeFormatInfo.CurrentInfo.GetMonthName(i));
            }
        }
    }
}