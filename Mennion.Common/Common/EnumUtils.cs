﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Mennion.Common.Common
{
    /// <summary>
    /// Utilities for working with <see cref="System.Enum"/> types.
    /// </summary>
    public static class EnumUtils
    {
        /// <summary>
        /// Converts the string representation of the name or numeric value of one or 
        /// more enumerated constants to an equivalent enumerated object.
        /// </summary>
        /// <typeparam name="TEnum">An enumeration type.</typeparam>
        /// <param name="value">A string containing the name or value to convert.</param>
        /// <returns>An object of type <typeparamref name="TEnum"/> whose value is represented by <paramref name="value"/>.</returns>
        public static TEnum ParseFromString<TEnum>(string value)
        {
            Ensure.Argument.NotNullOrEmpty(value, "enumString");
            return (TEnum)Enum.Parse(typeof(TEnum), value);
        }

        public static string Description<TEnum>(this TEnum enumObject)
        {
            Type type = enumObject.GetType();
            MemberInfo[] memInfo = type.GetMember(enumObject.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                var attributes = (DescriptionAttribute[])memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    return attributes[0].Description;
                }
            }

            return enumObject.ToString();

        }

    }
}
