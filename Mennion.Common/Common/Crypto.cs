﻿namespace Mennion.Common.Common
{
    public static class Crypto
    {
        public static string ComputeHash(string plainText)
        {
            var salt = BCrypt.GenerateSalt();
            var hash = BCrypt.HashPassword(plainText, salt);
            return hash;
        }

        public static bool VerifyHash(string plainText,string hash)
        {
            return BCrypt.Verify(plainText,hash);
        }


        public static string EncryptString(string plainText)
        {
            return Aes.EncryptStringAES(plainText);
        }

        public static string DecryptString(string decryptedText)
        {
            return Aes.DecryptStringAES(decryptedText);
        }


    }
}