﻿using System;
using FluentValidation;
using StructureMap;

namespace Mennion.Common.Web.IoC
{
    public class StructureMapValidatorFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type validatorType)
        {
            return ObjectFactory.TryGetInstance(validatorType) as IValidator;
        }
    }
}