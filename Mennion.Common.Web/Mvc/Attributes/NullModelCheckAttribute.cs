﻿using System.Web.Mvc;

namespace Mennion.Common.Web.Mvc.Attributes
{
    /// <summary>
    /// Attribut který slouží k zachycený prázdného (null)modelu
    /// posílaného ajaxem(get)
    /// </summary>
    public class NullModelCheckAttribute : ActionFilterAttribute
    {
        public string Description { get; set; }

        public NullModelCheckAttribute(string description = null)
        {
            Description = description;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var result = filterContext.Result as ViewResultBase;

            if (result != null && result.Model == null)
            {
                filterContext.Result = new JsonResult
                                           {
                                               Data = new {success = false, message = Description, resultType = "error"},
                                               JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                           };
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
