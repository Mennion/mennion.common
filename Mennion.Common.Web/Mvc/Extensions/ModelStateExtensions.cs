﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mennion.Common.Common;

namespace Mennion.Common.Web.Mvc.Extensions
{
    public static class ModelStateExtensions
    {
        /// <summary>
        /// Converts the <paramref name="modelState"/> to a string dictionary that can be easily serialized.
        /// </summary>
        public static IDictionary<string, string[]> ToSerializableDictionary(this ModelStateDictionary modelState)
        {
            Ensure.NotNull(modelState,"modelstate");
            return modelState.Where(x => x.Value.Errors.Any()).ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );
        }
    }
}
