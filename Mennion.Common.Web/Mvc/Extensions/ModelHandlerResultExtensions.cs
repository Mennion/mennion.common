﻿using System.Web.Mvc;
using Mennion.Common.Web.Mvc.Common;

namespace Mennion.Common.Web.Mvc.Extensions
{
    public static class ModelHandlerResultExtensions
    {
        public static JsonResult BuildJsonResult(this ModelHandlerResult result,object data = null)
        {
            return new JsonResult
            {
                Data = new
                {
                    success = true,
                    resultType = result.GetJsonAlertType(),
                    message = result.Message,
                    data
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public static JsonResult BuildHtmlResult(this ModelHandlerResult result, object data = null)
        {
            return new JsonResult
            {
                Data = new
                {
                    success = true,
                    resultType = result.GetJsonAlertType(),
                    message = result.Message,
                    data
                },
                ContentType = "text/html",
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}