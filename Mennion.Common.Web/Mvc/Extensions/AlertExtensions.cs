using Mennion.Common.Web.Mvc.Common;

namespace Mennion.Common.Web.Mvc.Extensions
{
    public static class AlertExtensions
    {
        /// <summary>
        /// Vr�t� typ upozorn�n� na z�klad� CommandHandleResult objektu
        /// </summary>
        /// <param name="result">CommandHandleResult objet</param>
        /// <returns>AlertType objekt</returns>
        public static AlertType GetAlertType(this ModelHandlerResult result)
        {
            return result.Success ? AlertType.Success : AlertType.Error;
        }

        /// <summary>
        /// Vr�t� typ upozorn�n� na z�klad� CommandHandleResult objektu ve stringu
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static string GetJsonAlertType(this ModelHandlerResult result)
        {
            return result.Success ? "success" : "error";
        }        
    }
}