﻿namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Rozhraní pro vytváření view model handlerů (třídy které se starají o zpracování view modelu)
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public interface IModelHandler<TModel>
    {
        ModelHandlerResult Handle(TModel viewModel);
    }

}