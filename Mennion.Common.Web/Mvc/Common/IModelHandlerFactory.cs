namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Rozhran� pro vytv��en� view model handler�
    /// </summary>
    public interface IModelHandlerFactory
    {
        TModelHandler Create<TModelHandler>();
    }
}