﻿namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Rozhraní pro vytváření view modelů
    /// </summary>
    public interface IModelBuilderFactory
    {
        /// <summary>
        /// Vytvoří view model (bez zadaného vstupu)
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        TModel Create<TModel>();

        /// <summary>
        /// Vytvoří view model z vstupu)
        /// </summary>
        /// <typeparam name="TInput">jednoduchý typ/objekt</typeparam>
        /// <typeparam name="TModel">view model</typeparam>
        /// <param name="input">hodnota vstupu</param>
        /// <returns>view model</returns>
        TModel Create<TInput, TModel>(TInput input);
    }
}
