using StructureMap;

namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Standardn� implementace rozhran� IModelHandlerFactory
    /// </summary>
    public class DefaultModelHandlerFactory : IModelHandlerFactory
    {
        /// <summary>
        /// Vytvo�� handler pro view model, kter� se vol� v controllerech
        /// </summary>
        /// <typeparam name="TVModelHandler">model handler</typeparam>
        /// <returns></returns>
        public TModelHandler Create<TModelHandler>()
        {
            return ObjectFactory.GetInstance<TModelHandler>();
        }
    }
}