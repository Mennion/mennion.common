﻿
namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Třída reprezentujicí upozornění pro uživatele
    /// Typ upozornění je uložen v enumu AlertType
    /// </summary>
    public class Alert
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public AlertType AlertType { get; set; }

        public Alert(AlertType alertType, string title, string description = null)
        {
            AlertType = alertType;
            Title = title; 
            Description = description;
        }

        public Alert() { }
    }

    public enum AlertType
    {
        Info,
        Success,
        Warning,
        Error
    }
}
