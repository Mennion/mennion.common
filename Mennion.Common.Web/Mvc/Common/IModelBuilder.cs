﻿namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Rozhraní pro sestavování view modelů
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public interface IModelBuilder<TModel>
    {
        /// <summary>
        /// Vytvoří view model
        /// </summary>
        /// <returns>objekt view modelu</returns>
        TModel Build();
    }

    /// <summary>
    /// Vytvoří view modelu ze vstupu (TInput)
    /// </summary>
    /// <returns>objekt view modelu</returns>    
    public interface IModelBuilder<TInput, TView>
    {
        TView Build(TInput input);
    }
}
