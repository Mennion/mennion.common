﻿using System;
using System.Web.Mvc;

namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Standardní implementace rozhraní IVIewModel factory
    /// </summary>
    public class DefaultModelBuilderFactory : IModelBuilderFactory
    {      
        public TModel Create<TModel>()
        {
            var builder = DependencyResolver.Current.GetService<IModelBuilder<TModel>>();

            if (builder != null)
                return builder.Build();

            // otherwise create view using reflection
            return Activator.CreateInstance<TModel>();
        }

        public TModel Create<TInput, TModel>(TInput input)
        {
            var builder = DependencyResolver.Current.GetService<IModelBuilder<TInput, TModel>>();

            if (builder != null)
                return builder.Build(input);

            // otherwise create using reflection
            return (TModel)Activator.CreateInstance(typeof(TModel), input);
        }
    }
}
