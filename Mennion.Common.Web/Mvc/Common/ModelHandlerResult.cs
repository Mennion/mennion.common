namespace Mennion.Common.Web.Mvc.Common
{
    /// <summary>
    /// Objekt, kter� reprezentuje n�vratov� stav po zpracov�n� viewmodelu handlerem
    /// </summary>
    public class ModelHandlerResult
    {
        public string Message { get; set; }
        public bool Success { get; set; }

        public ModelHandlerResult()
        {
            Success = true;
        }
    }
}