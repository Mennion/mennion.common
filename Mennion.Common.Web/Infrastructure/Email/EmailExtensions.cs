﻿using System;
using Mennion.Common.Infrastructure.Logging;

namespace Mennion.Common.Infrastructure.Email
{
    /// <summary>
    /// Rozšíření pro fluent email knihovnu pro posílání zpráv z možností logování chyb
    /// </summary>
    public static class EmailExtensions
    {
        /// <summary>
        /// Pokusí se odeslat email, pokud selže zachytí vyjímku, zaloguje jí a nepřeruší tok programu.
        /// </summary>
        /// <param name="email"></param>
        public static void TrySend(this global::FluentEmail.Email email)
        {
            try
            {
                //todo zde přijde kontrola jestli se e-maily mají vůbec odesílat
                email.Send();
            }
            catch (Exception e)
            {
                ObjectFactory.GetInstance<ILogger>().Error("Nepodařilo se odeslat e-mail", e);
            }
        }
    }
}