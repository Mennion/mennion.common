﻿using System;

namespace Mennion.Common.Web.Infrastructure.Cache
{
    /// <summary>
    /// Rozhraní pro práci s cachí v aplikaci.
    /// Pokud aplikace potřebuje využívat cache požádá si o instanci ICacheProvider
    /// </summary>
    public interface ICacheProvider
    {
        /// <summary>
        /// Vrátí objekt z kolekce keše. Pokud objekt v keši není uložen vrací null
        /// </summary>
        /// <typeparam name="T">Typ objektu který požadujeme dostat z keše</typeparam>
        /// <param name="key">Klíč pod kterým je objekt uložen v keši</param>
        /// <returns>Pokud klíč existuje v kolekci vrací objekt, jinak null</returns>
        T Get<T>(string key) where T:class;

        /// <summary>
        /// Uloží objet do kolekce keše pod zadaným klíčem.
        /// Pokud objekt již v keši existuje dojde k jeho přepsání
        /// </summary>
        /// <param name="key">Klíč pod kterým je objekt uložen v keši</param>
        /// <param name="cachedObject">Kešovaný objekt</param>
        /// <param name="expiration">Doba po kterou je keš držen v kolekci</param>
        void Store(string key, object cachedObject, DateTime expiration);

        /// <summary>
        /// Znevalidní objekt z kolekce pod zadaným klíčem.      
        /// </summary>
        /// <param name="key">Klíč pod kterým je objekt uložen v keši</param>
        void Remove(string key);

        /// <summary>
        /// Vyprazní celou kolekci
        /// </summary>
        void Clear();
    }
}