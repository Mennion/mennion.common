﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Mennion.Common.Web.Infrastructure.Cache
{
    /// <summary>
    /// Webová implementace keše pro aplikaci.
    /// Pro ukládání dat používá paměť webového serveru.
    /// Pokud se dostane server na kterém aplikace běží do stavu, kdy mu bude docházet operační paměť, kešování nebude fungovat.
    /// </summary>
    public class WebCacheProvider : ICacheProvider
    {
        /// <summary>
        /// Vrátí objekt z kolekce keše. Pokud objekt v keši není uložen vrací null
        /// </summary>
        /// <typeparam name="T">Typ objektu který požadujeme dostat z keše</typeparam>
        /// <param name="key">Klíč pod kterým je objekt uložen v keši</param>
        /// <returns>Pokud klíč existuje v kolekci vrací objekt, jinak null</returns>
        public T Get<T>(string key) where T : class
        {
            return HttpRuntime.Cache[key] as T;
        }

        /// <summary>
        /// Uloží objet do kolekce keše pod zadaným klíčem.
        /// Pokud objekt již v keši existuje dojde k jeho přepsání
        /// </summary>
        /// <param name="key">Klíč pod kterým je objekt uložen v keši</param>
        /// <param name="cachedObject">Kešovaný objekt</param>
        /// <param name="expiration">Doba po kterou je keš držen v kolekci</param>
        public void Store(string key, object cachedObject, DateTime expiration)
        {
            HttpRuntime.Cache.Insert(key,cachedObject,null,expiration,TimeSpan.Zero);
        }

        /// <summary>
        /// Znevalidní objekt z kolekce pod zadaným klíčem.      
        /// </summary>
        /// <param name="key">Klíč pod kterým je objekt uložen v keši</param>
        public void Remove(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        /// <summary>
        /// Vyprazní celou kolekci
        /// </summary>
        public void Clear()
        {
            var keys = new List<string>();
            var enumerator = HttpRuntime.Cache.GetEnumerator();

            while (enumerator.MoveNext())
            {
                keys.Add(enumerator.Key.ToString());
            }

            foreach (var t in keys)
            {
                HttpRuntime.Cache.Remove(t);
            }
        }
    }
}