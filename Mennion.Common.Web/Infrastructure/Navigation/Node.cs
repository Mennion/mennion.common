﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Mennion.Common.Web.Infrastructure.Navigation
{
	public class Node<TController> : NodeBase where TController : IController
	{
		IList<INode> _childNodes;

		public Node(Expression<Action<TController>> action) : this(action, null,null, new INode[0])
		{
		}

        public Node(Expression<Action<TController>> action,string title,string css) : this(action, title, css, new INode[0])
        {
        }

		public Node(Expression<Action<TController>> action, string title) : this(action, title,null, new INode[0])
		{
		}

		public Node(Expression<Action<TController>> action, params INode[] childNodes) : this(action, null,null, childNodes)
		{
		}

		public Node(Expression<Action<TController>> action, string title,string css, params INode[] childNodes)
		{
			if (action == null)
				throw new ArgumentNullException("action");

			if (childNodes == null)
				throw new ArgumentNullException("childNodes");

			var methodCallExpression = action.Body as MethodCallExpression;
			if (methodCallExpression == null)
				throw new ArgumentException("Node must be initialised with method call expression (e.g. controller => controller.Action())", "action");
		    Css = css;
			Initialise(methodCallExpression, title);
			InitialiseChildNodes(childNodes);
		}

		public override IEnumerable<INode> Children
		{
			get { return _childNodes; }
		}

		void InitialiseChildNodes(INode[] childNodes)
		{
			foreach (var childNode in childNodes)
				childNode.SetParent(this);

			_childNodes = new List<INode>(childNodes);
		}
	}

	public class Node<TController, TAreaRegistration> : Node<TController> where TController : IController where TAreaRegistration : AreaRegistration
	{
		public Node(Expression<Action<TController>> action) : this(action, null, null,new INode[0])
		{
		}

		public Node(Expression<Action<TController>> action, string title) : this(action, title,null, new INode[0])
		{
		}

        public Node(Expression<Action<TController>> action, string title,string css): this(action, title,css, new INode[0])
        {
        }


		public Node(Expression<Action<TController>> action, params INode[] childNodes) : this(action, null,null, childNodes)
		{
		}

		public Node(Expression<Action<TController>> action, string title,string css, params INode[] childNodes) : base(action, title,null, childNodes)
		{
			// ReSharper disable DoNotCallOverridableMethodsInConstructor
			AreaName = Activator.CreateInstance<TAreaRegistration>().AreaName;
			SetAreaName(RouteValues, AreaName);
		    Css = css;
		    // ReSharper restore DoNotCallOverridableMethodsInConstructor
		}
	}
}