using System.Collections.Generic;
using System.Web.Routing;

namespace Mennion.Common.Web.Infrastructure.Navigation
{
	public interface INode
	{
		string ActionName { get; }
		string Title { get; }
		string ControllerName { get; }
		string AreaName { get; }
        string Css { get; }
		RouteValueDictionary RouteValues { get; }
		INode Parent { get; }
		IEnumerable<INode> Children { get; }

		void SetParent(INode parent);
	}
}