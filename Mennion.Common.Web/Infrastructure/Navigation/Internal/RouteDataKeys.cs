namespace Mennion.Common.Web.Infrastructure.Navigation.Internal
{
	internal static class RouteDataKeys
	{
		internal const string Action = "action";
		internal const string Controller = "controller";
	}
}