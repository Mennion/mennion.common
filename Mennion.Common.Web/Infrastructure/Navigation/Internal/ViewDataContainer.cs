﻿using System.Web.Mvc;

namespace Mennion.Common.Web.Infrastructure.Navigation.Internal
{
	internal class ViewDataContainer<TModel> : IViewDataContainer
	{
		internal ViewDataContainer(TModel model)
		{
			ViewData = new ViewDataDictionary<TModel>(model);
		}

		#region IViewDataContainer Members

		public ViewDataDictionary ViewData { get; set; }

		#endregion
	}
}