﻿using System.Web.Mvc;

namespace Mennion.Common.Web.Infrastructure.Navigation.Sitemap
{
	public class XmlSitemapController : Controller
	{
		public ActionResult Sitemap()
		{
			return new XmlSitemapResult(Url);
		}
	}
}