﻿$(function () {
    $.fn.submitForm = function (success, error) {
        var form = $(this);
        var ajaxOptions = {

            beforeSubmit: function () {
                $('.field-validation-error').each(function () {
                    $(this).html("");
                });
            },
            success: function (data) {
                try {
                    var parsedData = JSON.parse(data);
                    if (parsedData.success == true) {
                        success(data);
                    } else {
                        var errors = JSON.parse(parsedData.errors);
                        $.each(errors, function (input, errorValue) {
                            var span = $("span[data-valmsg-for='" + input + "']");
                            if (span.length > 0) {
                                $(span).removeClass("field-validation-valid");
                                $(span).addClass("field-validation-error");
                                $(span).html(errorValue);
                            }
                        });
                        error(data);
                    }
                } catch (e) {
                }
            }
        };

        $(form).ajaxForm(ajaxOptions);
    };
});

