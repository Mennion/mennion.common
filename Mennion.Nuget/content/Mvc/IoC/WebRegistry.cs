﻿using System.Reflection;
using FluentValidation;
using Mennion.Common.Web.Infrastructure.Cache;
using Mennion.Common.Web.Infrastructure.Logging;
using Mennion.Common.Web.Mvc.Common;
using StructureMap.Configuration.DSL;

namespace Mennion.Nuget.content.Mvc.IoC
{
    public class WebRegistry : Registry
    {
        public WebRegistry()
        {
            Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.ConnectImplementationsToTypesClosing(typeof(IModelBuilder<>));
                scan.ConnectImplementationsToTypesClosing(typeof(IModelBuilder<,>));
                scan.WithDefaultConventions();
            });

            For<IModelBuilderFactory>().Use<DefaultModelBuilderFactory>();
            For<IModelHandlerFactory>().Use<DefaultModelHandlerFactory>();
            For<ICacheProvider>().Singleton().Use<WebCacheProvider>();

            // pouze při použítí nlogu
            For<ILogger>().Use<NLogLogger>().Ctor<string>("currentClassName").Is(c => c.BuildStack.Root.RequestedType.FullName);

            // pouze při použití validací
            AssemblyScanner.FindValidatorsInAssembly(Assembly.GetCallingAssembly()).ForEach(s =>
            {
                For(s.InterfaceType).HttpContextScoped().Use(s.ValidatorType);
            });

        }
    }
}
